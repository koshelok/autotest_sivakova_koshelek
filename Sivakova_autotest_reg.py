import unittest
import time
import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class Test_sivakova_auto(unittest.TestCase):
    def test_null(self):#Все поля пустые
        browser = webdriver.Chrome()
        browser.get('https://koshelek.ru/authorization/signup')
        WebDriverWait(browser,100).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'[tag="i"]')))# были проблемы с загрузкой  сайта
        sh_root=browser.find_element(By.CSS_SELECTOR,'[class="remoteComponent"]').shadow_root # вход в тень

        sh_root.find_element(By.CSS_SELECTOR,'input[type="checkbox"]').click()#чек бокс
        browser.implicitly_wait(5)
        button=sh_root.find_element(By.CSS_SELECTOR,'button[type="submit"]')
        button.click()#отправка формы
        fail_propusk=sh_root.find_elements(By.CSS_SELECTOR,'span[style="--text-color: var(--status-error-darken);"]')
        self.assertIn('Поле не заполнено' , fail_propusk[0].text) 
        self.assertIn('Поле не заполнено' , fail_propusk[1].text) 
        self.assertIn('Поле не заполнено' , fail_propusk[2].text) 
        browser.quit()
    
    def test_name_only(self):#Заполнено только имя
        browser = webdriver.Chrome()
        browser.get('https://koshelek.ru/authorization/signup')
        WebDriverWait(browser,100).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'[tag="i"]')))# были проблемы с загрузкой  сайта
        sh_root=browser.find_element(By.CSS_SELECTOR,'[class="remoteComponent"]').shadow_root # вход в тень
        name=sh_root.find_elements(By.CSS_SELECTOR,'input')[0]
        name.send_keys('Shura')# ввод имени на английском
        sh_root.find_element(By.CSS_SELECTOR,'input[type="checkbox"]').click()#чек бокс
        button=sh_root.find_element(By.CSS_SELECTOR,'button[type="submit"]')
        button.click()#отправка формы
        fail_propusk=sh_root.find_elements(By.CSS_SELECTOR,'span[style="--text-color: var(--status-error-darken);"]')
        self.assertIn('Поле не заполнено',fail_propusk[1].text)
        self.assertIn('Поле не заполнено',fail_propusk[2].text)#только имя
        browser.quit()
        

    def test_email_only(self):#Заполнена только почта
        browser = webdriver.Chrome()
        browser.get('https://koshelek.ru/authorization/signup')
        WebDriverWait(browser,100).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'[tag="i"]')))# были проблемы с загрузкой  сайта
        sh_root=browser.find_element(By.CSS_SELECTOR,'[class="remoteComponent"]').shadow_root # вход в тень
        emale=sh_root.find_elements(By.CSS_SELECTOR,'input')[1]
        emale.send_keys("rum@mail.ru")#ввод мыла
        sh_root.find_element(By.CSS_SELECTOR,'input[type="checkbox"]').click()#чек бокс
        button=sh_root.find_element(By.CSS_SELECTOR,'button[type="submit"]')
        button.click()#отправка формы
        fail_propusk=sh_root.find_elements(By.CSS_SELECTOR,'span[style="--text-color: var(--status-error-darken);"]')
        self.assertIn('Поле не заполнено', fail_propusk[0].text)
        self.assertIn('Поле не заполнено',fail_propusk[2].text)#только мыло
        browser.quit()


    def test_password_only(self):#Заполнен только пароль
        browser = webdriver.Chrome()
        browser.get('https://koshelek.ru/authorization/signup')
        WebDriverWait(browser,100).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'[tag="i"]')))# были проблемы с загрузкой  сайта
        sh_root=browser.find_element(By.CSS_SELECTOR,'[class="remoteComponent"]').shadow_root # вход в тень
        pasw=sh_root.find_elements(By.CSS_SELECTOR,'input')[2]
        pasw.send_keys('Pas321_!')#ввод пароля
        sh_root.find_element(By.CSS_SELECTOR,'input[type="checkbox"]').click()#чек бокс
        button=sh_root.find_element(By.CSS_SELECTOR,'button[type="submit"]')
        button.click()#отправка формы
        fail_propusk=sh_root.find_elements(By.CSS_SELECTOR,'span[style="--text-color: var(--status-error-darken);"]')
        self.assertIn('Поле не заполнено',fail_propusk[0].text)
        self.assertIn('Поле не заполнено',fail_propusk[1].text)#только пароль
        browser.quit()

    def test_no_name(self):#Заполнены все кроме имени
        browser = webdriver.Chrome()
        browser.get('https://koshelek.ru/authorization/signup')
        WebDriverWait(browser,100).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'[tag="i"]')))# были проблемы с загрузкой  сайта
        sh_root=browser.find_element(By.CSS_SELECTOR,'[class="remoteComponent"]').shadow_root # вход в тень
        emale=sh_root.find_elements(By.CSS_SELECTOR,'input')[1]
        emale.send_keys("rum@mail.ru")#ввод мыла
        pasw=sh_root.find_elements(By.CSS_SELECTOR,'input')[2]
        pasw.send_keys('Pas321_!')#ввод пароля
        sh_root.find_element(By.CSS_SELECTOR,'input[type="checkbox"]').click()#чек бокс
        button=sh_root.find_element(By.CSS_SELECTOR,'button[type="submit"]')
        button.click()#отправка формы
        fail_propusk=sh_root.find_elements(By.CSS_SELECTOR,'span[style="--text-color: var(--status-error-darken);"]')
        self.assertIn('Поле не заполнено',fail_propusk[0].text)
        browser.quit()

    def test_no_email(self):#Заполнены все кроме почты
        browser = webdriver.Chrome()
        browser.get('https://koshelek.ru/authorization/signup')
        WebDriverWait(browser,100).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'[tag="i"]')))# были проблемы с загрузкой  сайта
        sh_root=browser.find_element(By.CSS_SELECTOR,'[class="remoteComponent"]').shadow_root # вход в тень
        name=sh_root.find_elements(By.CSS_SELECTOR,'input')[0]
        name.send_keys('Shura')# ввод имени на английском
        pasw=sh_root.find_elements(By.CSS_SELECTOR,'input')[2]
        pasw.send_keys('Pas321_!')#ввод пароля
        sh_root.find_element(By.CSS_SELECTOR,'input[type="checkbox"]').click()#чек бокс
        button=sh_root.find_element(By.CSS_SELECTOR,'button[type="submit"]')
        button.click()#отправка формы
        fail_propusk=sh_root.find_elements(By.CSS_SELECTOR,'span[style="--text-color: var(--status-error-darken);"]')
        self.assertIn('Поле не заполнено',fail_propusk[1].text)
        browser.quit()

    def test_no_password(self):#Заполнены все кроме пароля  
        browser = webdriver.Chrome()
        browser.get('https://koshelek.ru/authorization/signup')
        WebDriverWait(browser,100).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'[tag="i"]')))# были проблемы с загрузкой  сайта
        sh_root=browser.find_element(By.CSS_SELECTOR,'[class="remoteComponent"]').shadow_root # вход в тень
        name=sh_root.find_elements(By.CSS_SELECTOR,'input')[0]
        name.send_keys('Shura')# ввод имени на английском
        emale=sh_root.find_elements(By.CSS_SELECTOR,'input')[1]
        emale.send_keys("rum@mail.ru")#ввод мыла
        sh_root.find_element(By.CSS_SELECTOR,'input[type="checkbox"]').click()#чек бокс
        button=sh_root.find_element(By.CSS_SELECTOR,'button[type="submit"]')
        button.click()#отправка формы
        time.sleep(2)
        fail_propusk=sh_root.find_elements(By.CSS_SELECTOR,'span[style="--text-color: var(--status-error-darken);"]')
        self.assertIn("Поле не заполнено",fail_propusk[2].text)
        browser.quit()

    def test_no_check(self):#Заполнено все ,но не нажать чек бокс
        browser = webdriver.Chrome()
        browser.get('https://koshelek.ru/authorization/signup')
        WebDriverWait(browser,100).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'[tag="i"]')))# были проблемы с загрузкой  сайта
        sh_root=browser.find_element(By.CSS_SELECTOR,'[class="remoteComponent"]').shadow_root # вход в тень
        name=sh_root.find_elements(By.CSS_SELECTOR,'input')[0]
        name.send_keys('Shura')# ввод имени на английском
        emale=sh_root.find_elements(By.CSS_SELECTOR,'input')[1]
        emale.send_keys("rum@mail.ru")#ввод мыла
        pasw=sh_root.find_elements(By.CSS_SELECTOR,'input')[2]
        pasw.send_keys('Pas321_!')#ввод пароля
        button=sh_root.find_element(By.CSS_SELECTOR,'button[type="submit"]')
        button.click()#отправка формы
        ch=sh_root.find_element(By.CSS_SELECTOR,'input[type="checkbox"]')        
        self.assertEqual( ch.get_attribute('aria-checked'),"false")
        browser.quit()


    def test_name_rus(self):#Имя на русском
        browser = webdriver.Chrome()
        browser.get('https://koshelek.ru/authorization/signup')
        WebDriverWait(browser,100).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'[tag="i"]')))# были проблемы с загрузкой  сайта
        sh_root=browser.find_element(By.CSS_SELECTOR,'[class="remoteComponent"]').shadow_root # вход в тень
        name=sh_root.find_elements(By.CSS_SELECTOR,'input')[0]
        name.send_keys('Шура')# ввод имени на русском
        emale=sh_root.find_elements(By.CSS_SELECTOR,'input')[1]
        emale.send_keys("rum@mail.ru")#ввод мыла
        pasw=sh_root.find_elements(By.CSS_SELECTOR,'input')[2]
        pasw.send_keys('Pas321_!')#ввод пароля
        sh_root.find_element(By.CSS_SELECTOR,'input[type="checkbox"]').click()#чек бокс
        button=sh_root.find_element(By.CSS_SELECTOR,'button[type="submit"]')
        button.click()#отправка формы
        fail_rus=sh_root.find_elements(By.CSS_SELECTOR,'span[style="--text-color: var(--status-error-darken);"]')
        self.assertIn("Допустимые символы (от 6 до 32): a-z, 0-9, _. Имя должно начинаться с буквы",fail_rus[0].text)
        browser.quit()


    def test_email_no_а(self):#Мыло не содержит @
        browser = webdriver.Chrome()
        browser.get('https://koshelek.ru/authorization/signup')
        WebDriverWait(browser,100).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'[tag="i"]')))# были проблемы с загрузкой  сайта
        sh_root=browser.find_element(By.CSS_SELECTOR,'[class="remoteComponent"]').shadow_root # вход в тень
        name=sh_root.find_elements(By.CSS_SELECTOR,'input')[0]
        name.send_keys('Shura')# ввод имени на англ
        emale=sh_root.find_elements(By.CSS_SELECTOR,'input')[1]
        emale.send_keys("rummail.ru")#ввод мыла без @
        pasw=sh_root.find_elements(By.CSS_SELECTOR,'input')[2]
        pasw.send_keys('Pas321_!')#ввод пароля
        sh_root.find_element(By.CSS_SELECTOR,'input[type="checkbox"]').click()#чек бокс
        button=sh_root.find_element(By.CSS_SELECTOR,'button[type="submit"]')
        button.click()#отправка формы
        fail_rus=sh_root.find_elements(By.CSS_SELECTOR,'span[style="--text-color: var(--status-error-darken);"]')
        self.assertIn("Формат e-mail: username@test.ru",fail_rus[1].text)
        browser.quit()

    def test_email_no_(self): #У мыла нет домена
        browser = webdriver.Chrome()
        browser.get('https://koshelek.ru/authorization/signup')
        WebDriverWait(browser,100).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'[tag="i"]')))# были проблемы с загрузкой  сайта
        sh_root=browser.find_element(By.CSS_SELECTOR,'[class="remoteComponent"]').shadow_root # вход в тень
        name=sh_root.find_elements(By.CSS_SELECTOR,'input')[0]
        name.send_keys('Shura')# ввод имени на англ
        emale=sh_root.find_elements(By.CSS_SELECTOR,'input')[1]
        emale.send_keys("rum@mailru")#ввод мыла без .
        pasw=sh_root.find_elements(By.CSS_SELECTOR,'input')[2]
        pasw.send_keys('Pas321_!')
        sh_root.find_element(By.CSS_SELECTOR,'input[type="checkbox"]').click()#чек бокс
        button=sh_root.find_element(By.CSS_SELECTOR,'button[type="submit"]')
        button.click()#отправка формы
        fail_rus=sh_root.find_elements(By.CSS_SELECTOR,'span[style="--text-color: var(--status-error-darken);"]')
        self.assertIn("Формат e-mail: username@test.ru",fail_rus[1].text)
        browser.quit()

       
    def test_password_3(self): #пароль из 3 цифр
        browser = webdriver.Chrome()
        browser.get('https://koshelek.ru/authorization/signup')
        WebDriverWait(browser,100).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'[tag="i"]')))# были проблемы с загрузкой  сайта
        sh_root=browser.find_element(By.CSS_SELECTOR,'[class="remoteComponent"]').shadow_root # вход в тень
        name=sh_root.find_elements(By.CSS_SELECTOR,'input')[0]
        name.send_keys('Shura')# ввод имени на англ
        emale=sh_root.find_elements(By.CSS_SELECTOR,'input')[1]
        emale.send_keys("rum@mail.ru")#ввод мыла 
        pasw=sh_root.find_elements(By.CSS_SELECTOR,'input')[2]
        pasw.send_keys('321')#ввод пароля
        sh_root.find_element(By.CSS_SELECTOR,'input[type="checkbox"]').click()#чек бокс
        button=sh_root.find_element(By.CSS_SELECTOR,'button[type="submit"]')
        button.click()#отправка формы 
        fail_rus=sh_root.find_elements(By.CSS_SELECTOR,'span[style="--text-color: var(--status-error-darken);"]')
        self.assertIn("Пароль должен содержать минимум 8 символов",fail_rus[2].text)
        browser.quit()

    def test_password_6(self): #пароль из 6 маленьких букв
        browser = webdriver.Chrome()
        browser.get('https://koshelek.ru/authorization/signup')
        WebDriverWait(browser,100).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'[tag="i"]')))# были проблемы с загрузкой  сайта
        sh_root=browser.find_element(By.CSS_SELECTOR,'[class="remoteComponent"]').shadow_root # вход в тень
        name=sh_root.find_elements(By.CSS_SELECTOR,'input')[0]
        name.send_keys('Shura')# ввод имени на англ
        emale=sh_root.find_elements(By.CSS_SELECTOR,'input')[1]
        emale.send_keys("rum@mail.ru")#ввод мыла 
        pasw=sh_root.find_elements(By.CSS_SELECTOR,'input')[2]
        pasw.send_keys('qwerty')#ввод пароля
        sh_root.find_element(By.CSS_SELECTOR,'input[type="checkbox"]').click()#чек бокс
        button=sh_root.find_element(By.CSS_SELECTOR,'button[type="submit"]')
        button.click()#отправка формы
        fail_rus=sh_root.find_elements(By.CSS_SELECTOR,'span[style="--text-color: var(--status-error-darken);"]')
        self.assertIn("Пароль должен содержать минимум 8 символов",fail_rus[2].text)
        browser.quit()



if __name__ == "__main__":
    unittest.main()