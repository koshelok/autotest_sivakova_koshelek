# Проверка негативных сценариев на странице ристрации
  Проверяются следующие сценарии:
  + Все поля пустые
  + Заполнено только имя
  + Заполнена только почта
  + Заполнен только пароль
  + Заполнены все кроме имени
  + Заполнены все кроме почты
  + Заполнены все кроме пароля
  + Заполнено все ,но не нажат чек бокс 
  + Имя на русском
  + Мыло не содержит @
  + У мыла нет домена
  + пароль из 3 цифр
  + пароль из 6 маленьких букв

Перед запуском установить внешнюю среду.
Тестировать можно как с помощью unittest, так и с помощью pytest.

## Установка внешней среды.
Файл Sivakova_test.txt добавлен в архив.
1. Распокавать файл Sivakova_test.txt туда же, где будет находится исполняемый код.
2. В terminal ввести последовательно следующие коменды:
+ ``` -m venv env```
+ ``` Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser```
+ ``` env\Scripts\activate.ps1```
+ ``` pip install -r Sivakova_test.txt```
3. Установить Selenium, pytest. Для этого в terminal ввести последовательно следующие коменды:
+ ``` pip install selenium```
+ ``` pip install pytest```

## Тело самого теста
Запускать можно как unittest или как pytest

``` python
import unittest
import time
import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class Test_sivakova_auto(unittest.TestCase):
    def test_null(self):#Все поля пустые
        browser = webdriver.Chrome()
        browser.get('https://koshelek.ru/authorization/signup')
        WebDriverWait(browser,100).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'[tag="i"]')))# были проблемы с загрузкой  сайта
        sh_root=browser.find_element(By.CSS_SELECTOR,'[class="remoteComponent"]').shadow_root # вход в тень

        sh_root.find_element(By.CSS_SELECTOR,'input[type="checkbox"]').click()#чек бокс
        browser.implicitly_wait(5)
        button=sh_root.find_element(By.CSS_SELECTOR,'button[type="submit"]')
        button.click()#отправка формы
        fail_propusk=sh_root.find_elements(By.CSS_SELECTOR,'span[style="--text-color: var(--status-error-darken);"]')
        self.assertIn('Поле не заполнено' , fail_propusk[0].text) 
        self.assertIn('Поле не заполнено' , fail_propusk[1].text) 
        self.assertIn('Поле не заполнено' , fail_propusk[2].text) 
        browser.quit()
    
    def test_name_only(self):#Заполнено только имя
        browser = webdriver.Chrome()
        browser.get('https://koshelek.ru/authorization/signup')
        WebDriverWait(browser,100).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'[tag="i"]')))# были проблемы с загрузкой  сайта
        sh_root=browser.find_element(By.CSS_SELECTOR,'[class="remoteComponent"]').shadow_root # вход в тень
        name=sh_root.find_elements(By.CSS_SELECTOR,'input')[0]
        name.send_keys('Shura')# ввод имени на английском
        sh_root.find_element(By.CSS_SELECTOR,'input[type="checkbox"]').click()#чек бокс
        button=sh_root.find_element(By.CSS_SELECTOR,'button[type="submit"]')
        button.click()#отправка формы
        fail_propusk=sh_root.find_elements(By.CSS_SELECTOR,'span[style="--text-color: var(--status-error-darken);"]')
        self.assertIn('Поле не заполнено',fail_propusk[1].text)
        self.assertIn('Поле не заполнено',fail_propusk[2].text)#только имя
        browser.quit()
        

    def test_email_only(self):#Заполнена только почта
        browser = webdriver.Chrome()
        browser.get('https://koshelek.ru/authorization/signup')
        WebDriverWait(browser,100).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'[tag="i"]')))# были проблемы с загрузкой  сайта
        sh_root=browser.find_element(By.CSS_SELECTOR,'[class="remoteComponent"]').shadow_root # вход в тень
        emale=sh_root.find_elements(By.CSS_SELECTOR,'input')[1]
        emale.send_keys("rum@mail.ru")#ввод мыла
        sh_root.find_element(By.CSS_SELECTOR,'input[type="checkbox"]').click()#чек бокс
        button=sh_root.find_element(By.CSS_SELECTOR,'button[type="submit"]')
        button.click()#отправка формы
        fail_propusk=sh_root.find_elements(By.CSS_SELECTOR,'span[style="--text-color: var(--status-error-darken);"]')
        self.assertIn('Поле не заполнено', fail_propusk[0].text)
        self.assertIn('Поле не заполнено',fail_propusk[2].text)#только мыло
        browser.quit()


    def test_password_only(self):#Заполнен только пароль
        browser = webdriver.Chrome()
        browser.get('https://koshelek.ru/authorization/signup')
        WebDriverWait(browser,100).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'[tag="i"]')))# были проблемы с загрузкой  сайта
        sh_root=browser.find_element(By.CSS_SELECTOR,'[class="remoteComponent"]').shadow_root # вход в тень
        pasw=sh_root.find_elements(By.CSS_SELECTOR,'input')[2]
        pasw.send_keys('Pas321_!')#ввод пароля
        sh_root.find_element(By.CSS_SELECTOR,'input[type="checkbox"]').click()#чек бокс
        button=sh_root.find_element(By.CSS_SELECTOR,'button[type="submit"]')
        button.click()#отправка формы
        fail_propusk=sh_root.find_elements(By.CSS_SELECTOR,'span[style="--text-color: var(--status-error-darken);"]')
        self.assertIn('Поле не заполнено',fail_propusk[0].text)
        self.assertIn('Поле не заполнено',fail_propusk[1].text)#только пароль
        browser.quit()

    def test_no_name(self):#Заполнены все кроме имени
        browser = webdriver.Chrome()
        browser.get('https://koshelek.ru/authorization/signup')
        WebDriverWait(browser,100).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'[tag="i"]')))# были проблемы с загрузкой  сайта
        sh_root=browser.find_element(By.CSS_SELECTOR,'[class="remoteComponent"]').shadow_root # вход в тень
        emale=sh_root.find_elements(By.CSS_SELECTOR,'input')[1]
        emale.send_keys("rum@mail.ru")#ввод мыла
        pasw=sh_root.find_elements(By.CSS_SELECTOR,'input')[2]
        pasw.send_keys('Pas321_!')#ввод пароля
        sh_root.find_element(By.CSS_SELECTOR,'input[type="checkbox"]').click()#чек бокс
        button=sh_root.find_element(By.CSS_SELECTOR,'button[type="submit"]')
        button.click()#отправка формы
        fail_propusk=sh_root.find_elements(By.CSS_SELECTOR,'span[style="--text-color: var(--status-error-darken);"]')
        self.assertIn('Поле не заполнено',fail_propusk[0].text)
        browser.quit()

    def test_no_email(self):#Заполнены все кроме почты
        browser = webdriver.Chrome()
        browser.get('https://koshelek.ru/authorization/signup')
        WebDriverWait(browser,100).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'[tag="i"]')))# были проблемы с загрузкой  сайта
        sh_root=browser.find_element(By.CSS_SELECTOR,'[class="remoteComponent"]').shadow_root # вход в тень
        name=sh_root.find_elements(By.CSS_SELECTOR,'input')[0]
        name.send_keys('Shura')# ввод имени на английском
        pasw=sh_root.find_elements(By.CSS_SELECTOR,'input')[2]
        pasw.send_keys('Pas321_!')#ввод пароля
        sh_root.find_element(By.CSS_SELECTOR,'input[type="checkbox"]').click()#чек бокс
        button=sh_root.find_element(By.CSS_SELECTOR,'button[type="submit"]')
        button.click()#отправка формы
        fail_propusk=sh_root.find_elements(By.CSS_SELECTOR,'span[style="--text-color: var(--status-error-darken);"]')
        self.assertIn('Поле не заполнено',fail_propusk[1].text)
        browser.quit()

    def test_no_password(self):#Заполнены все кроме пароля  
        browser = webdriver.Chrome()
        browser.get('https://koshelek.ru/authorization/signup')
        WebDriverWait(browser,100).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'[tag="i"]')))# были проблемы с загрузкой  сайта
        sh_root=browser.find_element(By.CSS_SELECTOR,'[class="remoteComponent"]').shadow_root # вход в тень
        name=sh_root.find_elements(By.CSS_SELECTOR,'input')[0]
        name.send_keys('Shura')# ввод имени на английском
        emale=sh_root.find_elements(By.CSS_SELECTOR,'input')[1]
        emale.send_keys("rum@mail.ru")#ввод мыла
        sh_root.find_element(By.CSS_SELECTOR,'input[type="checkbox"]').click()#чек бокс
        button=sh_root.find_element(By.CSS_SELECTOR,'button[type="submit"]')
        button.click()#отправка формы
        time.sleep(2)
        fail_propusk=sh_root.find_elements(By.CSS_SELECTOR,'span[style="--text-color: var(--status-error-darken);"]')
        self.assertIn("Поле не заполнено",fail_propusk[2].text)
        browser.quit()

    def test_no_check(self):#Заполнено все ,но не нажать чек бокс
        browser = webdriver.Chrome()
        browser.get('https://koshelek.ru/authorization/signup')
        WebDriverWait(browser,100).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'[tag="i"]')))# были проблемы с загрузкой  сайта
        sh_root=browser.find_element(By.CSS_SELECTOR,'[class="remoteComponent"]').shadow_root # вход в тень
        name=sh_root.find_elements(By.CSS_SELECTOR,'input')[0]
        name.send_keys('Shura')# ввод имени на английском
        emale=sh_root.find_elements(By.CSS_SELECTOR,'input')[1]
        emale.send_keys("rum@mail.ru")#ввод мыла
        pasw=sh_root.find_elements(By.CSS_SELECTOR,'input')[2]
        pasw.send_keys('Pas321_!')#ввод пароля
        button=sh_root.find_element(By.CSS_SELECTOR,'button[type="submit"]')
        button.click()#отправка формы
        ch=sh_root.find_element(By.CSS_SELECTOR,'input[type="checkbox"]')        
        self.assertEqual( ch.get_attribute('aria-checked'),"false")
        browser.quit()


    def test_name_rus(self):#Имя на русском
        browser = webdriver.Chrome()
        browser.get('https://koshelek.ru/authorization/signup')
        WebDriverWait(browser,100).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'[tag="i"]')))# были проблемы с загрузкой  сайта
        sh_root=browser.find_element(By.CSS_SELECTOR,'[class="remoteComponent"]').shadow_root # вход в тень
        name=sh_root.find_elements(By.CSS_SELECTOR,'input')[0]
        name.send_keys('Шура')# ввод имени на русском
        emale=sh_root.find_elements(By.CSS_SELECTOR,'input')[1]
        emale.send_keys("rum@mail.ru")#ввод мыла
        pasw=sh_root.find_elements(By.CSS_SELECTOR,'input')[2]
        pasw.send_keys('Pas321_!')#ввод пароля
        sh_root.find_element(By.CSS_SELECTOR,'input[type="checkbox"]').click()#чек бокс
        button=sh_root.find_element(By.CSS_SELECTOR,'button[type="submit"]')
        button.click()#отправка формы
        fail_rus=sh_root.find_elements(By.CSS_SELECTOR,'span[style="--text-color: var(--status-error-darken);"]')
        self.assertIn("Допустимые символы (от 6 до 32): a-z, 0-9, _. Имя должно начинаться с буквы",fail_rus[0].text)
        browser.quit()


    def test_email_no_а(self):#Мыло не содержит @
        browser = webdriver.Chrome()
        browser.get('https://koshelek.ru/authorization/signup')
        WebDriverWait(browser,100).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'[tag="i"]')))# были проблемы с загрузкой  сайта
        sh_root=browser.find_element(By.CSS_SELECTOR,'[class="remoteComponent"]').shadow_root # вход в тень
        name=sh_root.find_elements(By.CSS_SELECTOR,'input')[0]
        name.send_keys('Shura')# ввод имени на англ
        emale=sh_root.find_elements(By.CSS_SELECTOR,'input')[1]
        emale.send_keys("rummail.ru")#ввод мыла без @
        pasw=sh_root.find_elements(By.CSS_SELECTOR,'input')[2]
        pasw.send_keys('Pas321_!')#ввод пароля
        sh_root.find_element(By.CSS_SELECTOR,'input[type="checkbox"]').click()#чек бокс
        button=sh_root.find_element(By.CSS_SELECTOR,'button[type="submit"]')
        button.click()#отправка формы
        fail_rus=sh_root.find_elements(By.CSS_SELECTOR,'span[style="--text-color: var(--status-error-darken);"]')
        self.assertIn("Формат e-mail: username@test.ru",fail_rus[1].text)
        browser.quit()

    def test_email_no_(self): #У мыла нет домена
        browser = webdriver.Chrome()
        browser.get('https://koshelek.ru/authorization/signup')
        WebDriverWait(browser,100).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'[tag="i"]')))# были проблемы с загрузкой  сайта
        sh_root=browser.find_element(By.CSS_SELECTOR,'[class="remoteComponent"]').shadow_root # вход в тень
        name=sh_root.find_elements(By.CSS_SELECTOR,'input')[0]
        name.send_keys('Shura')# ввод имени на англ
        emale=sh_root.find_elements(By.CSS_SELECTOR,'input')[1]
        emale.send_keys("rum@mailru")#ввод мыла без .
        pasw=sh_root.find_elements(By.CSS_SELECTOR,'input')[2]
        pasw.send_keys('Pas321_!')
        sh_root.find_element(By.CSS_SELECTOR,'input[type="checkbox"]').click()#чек бокс
        button=sh_root.find_element(By.CSS_SELECTOR,'button[type="submit"]')
        button.click()#отправка формы
        fail_rus=sh_root.find_elements(By.CSS_SELECTOR,'span[style="--text-color: var(--status-error-darken);"]')
        self.assertIn("Формат e-mail: username@test.ru",fail_rus[1].text)
        browser.quit()

       
    def test_password_3(self): #пароль из 3 цифр
        browser = webdriver.Chrome()
        browser.get('https://koshelek.ru/authorization/signup')
        WebDriverWait(browser,100).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'[tag="i"]')))# были проблемы с загрузкой  сайта
        sh_root=browser.find_element(By.CSS_SELECTOR,'[class="remoteComponent"]').shadow_root # вход в тень
        name=sh_root.find_elements(By.CSS_SELECTOR,'input')[0]
        name.send_keys('Shura')# ввод имени на англ
        emale=sh_root.find_elements(By.CSS_SELECTOR,'input')[1]
        emale.send_keys("rum@mail.ru")#ввод мыла 
        pasw=sh_root.find_elements(By.CSS_SELECTOR,'input')[2]
        pasw.send_keys('321')#ввод пароля
        sh_root.find_element(By.CSS_SELECTOR,'input[type="checkbox"]').click()#чек бокс
        button=sh_root.find_element(By.CSS_SELECTOR,'button[type="submit"]')
        button.click()#отправка формы 
        fail_rus=sh_root.find_elements(By.CSS_SELECTOR,'span[style="--text-color: var(--status-error-darken);"]')
        self.assertIn("Пароль должен содержать минимум 8 символов",fail_rus[2].text)
        browser.quit()

    def test_password_6(self): #пароль из 6 маленьких букв
        browser = webdriver.Chrome()
        browser.get('https://koshelek.ru/authorization/signup')
        WebDriverWait(browser,100).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'[tag="i"]')))# были проблемы с загрузкой  сайта
        sh_root=browser.find_element(By.CSS_SELECTOR,'[class="remoteComponent"]').shadow_root # вход в тень
        name=sh_root.find_elements(By.CSS_SELECTOR,'input')[0]
        name.send_keys('Shura')# ввод имени на англ
        emale=sh_root.find_elements(By.CSS_SELECTOR,'input')[1]
        emale.send_keys("rum@mail.ru")#ввод мыла 
        pasw=sh_root.find_elements(By.CSS_SELECTOR,'input')[2]
        pasw.send_keys('qwerty')#ввод пароля
        sh_root.find_element(By.CSS_SELECTOR,'input[type="checkbox"]').click()#чек бокс
        button=sh_root.find_element(By.CSS_SELECTOR,'button[type="submit"]')
        button.click()#отправка формы
        fail_rus=sh_root.find_elements(By.CSS_SELECTOR,'span[style="--text-color: var(--status-error-darken);"]')
        self.assertIn("Пароль должен содержать минимум 8 символов",fail_rus[2].text)
        browser.quit()



if __name__ == "__main__":
    unittest.main()
```


## Копия содержимого файла Sivakova_test.txt.
```
attrs==23.2.0
certifi==2024.2.2
cffi==1.16.0
colorama==0.4.6
h11==0.14.0
idna==3.7
iniconfig==2.0.0
outcome==1.3.0.post0
packaging==24.0
pluggy==1.5.0
pycparser==2.22
PySocks==1.7.1
pytest==8.2.0
selenium==4.20.0
sniffio==1.3.1
sortedcontainers==2.4.0
trio==0.25.0
trio-websocket==0.11.1
typing_extensions==4.11.0
urllib3==2.2.1
wsproto==1.2.0
```